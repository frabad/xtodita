<?xml version="1.0" encoding="utf-8" ?>
<p:declare-step version="1.0" name="xml2dita"
  xmlns:p="http://www.w3.org/ns/xproc">

  <p:input port="source" sequence="false"/>
  <p:input port="parameters" kind="parameter"/>

  <p:xinclude/>
  
  <p:xslt name="cargo">
    <p:input port="stylesheet">
      <p:document href="xslt/cargo.xsl"/>
    </p:input>
  </p:xslt>
  
  <p:xslt name="halfway">
    <p:input port="stylesheet">
      <p:document href="xslt/halfway.xsl"/>
    </p:input>
  </p:xslt>

  <p:xslt name="fearless">
    <p:input port="stylesheet">
      <p:document href="xslt/fearless.xsl"/>
    </p:input>
  </p:xslt>

  <p:xslt name="heatstroke">
    <p:input port="stylesheet">
      <p:document href="xslt/heatstroke.xsl"/>
    </p:input>
  </p:xslt>
  
  <p:xslt name="hideaway">
    <p:input port="stylesheet">
      <p:document href="xslt/hideaway.xsl"/>
    </p:input>
  </p:xslt>
  
  <p:xslt name="cissors">
    <p:input port="stylesheet">
      <p:document href="xslt/cissors.xsl"/>
    </p:input>
  </p:xslt>
  
  <p:store name="ditamap"
    encoding="utf-8"
    indent="true"
    omit-xml-declaration="false"
    doctype-public="-//OASIS//DTD DITA Map//EN"
    doctype-system="map.dtd">
    <p:with-option name="href"
      select="concat(/*[1]/@id,'.ditamap')"
    />
    <p:input port="source">
      <p:pipe step="cissors" port="result"/>
    </p:input>
  </p:store>
  
  <p:for-each name="topics">
    <p:iteration-source>
      <p:pipe step="cissors" port="secondary"/>
    </p:iteration-source>
    <p:store
      encoding="utf-8"
      indent="true"
      omit-xml-declaration="false">
      <p:with-option name="doctype-public"
        select="concat(
            '-//OASIS//DTD DITA ',
            upper-case(substring(local-name(/*[1]),1,1)),
            substring(local-name(/*[1]),2),
            '//EN'
        )"
      />
      <p:with-option name="doctype-system"
        select="concat(local-name(/*[1]),'.dtd')"
      />
      <p:with-option name="href"
        select="concat(local-name(/*[1]),'/',/*[1]/@id,'.dita')"
      />
    </p:store>
  </p:for-each>

</p:declare-step>

