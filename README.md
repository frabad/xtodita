# xToDITA

a transformation pipeline based on [XSLT 2.0](https://www.w3.org/TR/xslt20/)
and [XProc](https://xproc.org/) for converting XML documents
to [DITA](https://www.oasis-open.org/committees/dita) 


## How to use

    xproc -i input.xml xtodita.xpl

where:

`xproc`
:   is the name of an XProc processor.
    When in doubt, please use [Calabash](http://xmlcalabash.com/).

`input.xml`
:   is the input XML document

The output DITA map is written to the current folder.
Every output DITA topic will be written to a topic-type folder.


## How it works

The provided pipeline is roughly a sequence of transformation steps. Each
step refers to an XSLT sheet with the same name plus the `.xsl` extension.

step | description
  ---|---
cargo | rename nodes and attribute-nodes to lower case
halfway | reorganize/improve the input tree, alledgedly making it non model-compliant but easier to transform to DITA
fearless | translate input elements and create DITA topic structures
heatstroke | rewrap topic types to specialized ones
hideaway | post-fix tree structures, make them fully DITA-compliant
cissors | re-identify, chunk, draw a map references for all topics
quantum, fringe, pyramid, airspace | not used yet ; these names are available for further steps
