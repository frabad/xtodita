<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:my="functions:my"
  exclude-result-prefixes="#all">

  <!-- make a list of characters -->
  <xsl:function name="my:charsplit" as="xs:string+">
    <xsl:param name="string" as="xs:string"/>
    <xsl:analyze-string select="$string" regex=".">
      <xsl:matching-substring>
        <xsl:value-of select="." />
      </xsl:matching-substring>
    </xsl:analyze-string>
  </xsl:function>
  
  <!-- index of human-readable topic IDs -->
  <xsl:variable name="topics" as="element()+">
    <xsl:for-each select="//*[title]">
      <topic>
        <xsl:attribute name="id" select="@id" />
        <xsl:attribute name="mrid" select="generate-id(.)" />
        <xsl:attribute name="hrid">
          <xsl:apply-templates mode="hrid" select="title" />
        </xsl:attribute>
      </topic>
    </xsl:for-each>
  </xsl:variable>
  
  <!--
    process every @id attribute with the id mode
  -->
  <xsl:template match="@id">
    <xsl:attribute name="id">
      <xsl:apply-templates mode="id" select="parent::*"/>
    </xsl:attribute>
  </xsl:template>

  <!--
    set the hrid mode as the default for the id mode
  -->
  <xsl:template mode="id" match="*">
    <xsl:apply-templates select="." mode="hrid"/>
  </xsl:template>

  <!-- id processing mode
    
    let everything that has an @id attribute
    support either @id copying and @id rewriting
  -->
  <xsl:template as="xs:string" mode="id" match="*[title|@id]">
    <xsl:choose>
      <xsl:when test="$id.rewrite">
        <xsl:variable as="xs:string*" name="categories">
          <xsl:apply-templates mode="categories" select="."/>
        </xsl:variable>
        <xsl:variable as="xs:string" name="hrid">
          <xsl:apply-templates mode="hrid" select="."/>
        </xsl:variable>
        <xsl:variable as="xs:string?" name="mrid">
          <xsl:if test="count($topics[@hrid=$hrid]) gt 1">
            <xsl:apply-templates mode="mrid" select="."/>
          </xsl:if>
        </xsl:variable>
        <xsl:value-of select="string-join(($categories,$hrid,$mrid),'-')" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="@id"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!-- categories
    
    help rewriting an ID by adding keywords based on available categories
  -->
  <xsl:template as="xs:string?" mode="categories" match="*[title|@id]">
    <xsl:variable as="xs:string*" name="terms">
      <xsl:for-each select="ancestor-or-self::*/prolog/metadata/category">
        <xsl:apply-templates select="." mode="hrid"/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:if test="count($terms) gt 0">
      <xsl:value-of select="string-join($terms,'-')" />
    </xsl:if>
  </xsl:template>
  
  <!-- machine-readable ID mode -->
  <xsl:template as="xs:string" mode="mrid" match="*[title|@id]">
    <xsl:value-of select="generate-id(.)" />
  </xsl:template>
  
  <!-- Human-Readable ID mode
    
    Let anything output a unique id string. The id string is the result
    of munging and normalizing words that are extracted from title or self.
  -->
  <xsl:template as="xs:string" mode="hrid" match="@* | *" name="hrid">
    <xsl:param name="phrase" select="(title, ., '_undefined')[1]"/>
    <xsl:variable name="stopwords">
      <xsl:variable name="lang" as="xs:string">
        <xsl:apply-templates select="." mode="lang" />
      </xsl:variable>
      <xsl:variable name="stopwords.en"
      >a an and as at in of or on the for while by</xsl:variable>
      <xsl:variable name="stopwords.it"
      >d di da del dell della delle degli in nel nello nella nei nelle
      il lo il la le i gl a al alla per e o su sul sulla tra</xsl:variable>
      <xsl:choose>
        <xsl:when test="$lang='en'">
          <xsl:value-of select="normalize-space($stopwords.en)" />
        </xsl:when>
        <xsl:when test="$lang='it'">
          <xsl:value-of select="normalize-space($stopwords.it)" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="normalize-space($stopwords.en)" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="phrase.munged">
      <xsl:variable name="phrase.separated">
        <xsl:variable name="separators">'"`-\/&amp;</xsl:variable>
        <xsl:value-of select="translate(
          $phrase,$separators,string-join(
            for $i in (1 to string-length($separators)) return ' ')
          )"/>
      </xsl:variable>
      <xsl:variable as="xs:string+" name="words"
        select="tokenize(lower-case(normalize-space($phrase.separated)), ' ')"
      />
      <xsl:for-each select="$words">
        <xsl:variable as="xs:string?" name="word.clean">
          <xsl:variable name="word.ascii">
            <xsl:variable name="letters.ko">äâàáëêèéïîìíöôòóüûùú</xsl:variable>
            <xsl:variable name="letters.ok">aaaaeeeeiiiioooouuuu</xsl:variable>
            <xsl:value-of select="translate(.,
              normalize-space($letters.ko),normalize-space($letters.ok)
            )"/>
          </xsl:variable>
          <xsl:variable name="chars.clean" as="xs:string*">
            <xsl:variable name="chars.ascii"
            >abcdefghijklmnopqrstuvwxyz0123456789_</xsl:variable>
            <xsl:for-each select="my:charsplit($word.ascii)">
              <xsl:if test="contains($chars.ascii, .)">
                <xsl:value-of select="."/>
              </xsl:if>
            </xsl:for-each>
          </xsl:variable>
          <xsl:value-of select="string-join($chars.clean)" />
        </xsl:variable>
        <xsl:if test="not(contains(
            concat(' ', $stopwords, ' '), concat(' ', $word.clean, ' ')))">
          <xsl:value-of select="concat($word.clean, ' ')"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:value-of
      select="translate(normalize-space($phrase.munged), ' ', '-')" />
  </xsl:template>

</xsl:stylesheet>

