<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:my="functions:my"
  exclude-result-prefixes="#all">
    
  <!--
    fix links URLs to have them match the IDs in the new chunked topics
  -->
  <xsl:template match="@href | @conref">
    <xsl:variable name="peer" select="substring-before(., '#')"/>
    <xsl:variable name="nondita"
      select="
        (../@format and not(contains(../@format, 'dita'))) or
        ../@scope = 'external' or parent::*[self::image]"/>
    <xsl:variable name="id.scoped"
      select="substring-after(substring-after(., '#'), '/')"/>
    <xsl:variable name="id">
      <xsl:choose>
        <xsl:when test="$nondita"/>
        <xsl:when test="$id.scoped != ''">
          <xsl:value-of select="substring-after(
            substring-before(., concat('/', $id.scoped)), '#'
          )" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="substring-after(., '#')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="dirname">
      <xsl:variable name="basename">
        <xsl:call-template name="uri-basename">
          <xsl:with-param name="uri" select="."/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:value-of select="substring-before(., $basename)"/>
    </xsl:variable>
    <xsl:variable name="id.normalized">
      <xsl:choose>
        <xsl:when test="$nondita"/>
        <xsl:when test="$peer != ''">
          <xsl:apply-templates mode="id"
            select="(document($peer, /)//*[@id = $id])[1]"/>
        </xsl:when>
        <xsl:when test="$id = ''">
          <xsl:apply-templates mode="id"
            select="(document(., /)//*[@id])[1]"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates mode="id" select="(//*[@id = $id])[1]"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:attribute name="{name(.)}">
      <xsl:choose>
        <xsl:when test="$nondita or $id.normalized = ''">
          <xsl:value-of select="."/>
        </xsl:when>
        <xsl:when test="$peer != ''">
          <xsl:value-of select="concat($peer, '#', $id.normalized)"/>
          <xsl:if test="$id.scoped != ''">
            <xsl:value-of select="concat('/', $id.scoped)"/>
          </xsl:if>
        </xsl:when>
        <xsl:when test="starts-with(., '#')">
          <xsl:value-of select="concat($id.normalized, '.dita')"/>
          <xsl:if test="$id.scoped != ''">
            <xsl:value-of
              select="concat('#', $id.normalized, '/', $id.scoped)"/>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($dirname, $id.normalized, '.dita')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>

  <xsl:template name="uri-basename">
    <xsl:param name="uri"/>
    <xsl:choose>
      <xsl:when test="contains($uri, '/')">
        <xsl:call-template name="uri-basename">
          <xsl:with-param name="uri" select="substring-after($uri, '/')"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$uri"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

