<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:my="functions:my"
  exclude-result-prefixes="#all">
  
  <!--
    no output in topicref mode
  -->
  <xsl:template match="@* | node()" mode="topicref" />
  
  <!-- 
    let topicrefs default to the topicref mode
  -->
  <xsl:template match="topicref">
    <xsl:apply-templates select="." mode="topicref"/>
  </xsl:template>

  <!-- 
    conditionally create a topicref from a linked topic in topicref mode
  -->
  <xsl:template match="topicref" mode="topicref">
    <xsl:if test="@href">
      <xsl:apply-templates mode="topicref" select="document(@href, /)/*[1]">
        <xsl:with-param name="topicmeta" select="topicmeta"/>
        <xsl:with-param name="topicref">
          <xsl:apply-templates select="topicref"/>
        </xsl:with-param>
      </xsl:apply-templates>
    </xsl:if>
  </xsl:template>

  <!--
    unwrap elements with specific @outputclass
    
    see $unwrap.outputclass parameter
  -->
  <xsl:template mode="topicref" priority="1" match="*[
    @outputclass[
      normalize-space(.) != '' and
      normalize-space(.) = tokenize($unwrap.outputclass, ' ')
    ]]">
    <xsl:apply-templates mode="topicref"/>
  </xsl:template>
  
  <!--
    create a topicref for every topic
  -->
  <xsl:template match="topic | reference | concept | task" mode="topicref">
    <xsl:param name="topicref" as="element()*"/>
    <xsl:param name="topicmeta" as="element(topicmeta)?"/>
    <xsl:variable name="_topicmeta" as="element(topicmeta)">
      <topicmeta>
        <xsl:if test="$topicref.navtitle and not($topicmeta/navtitle)">
          <navtitle><xsl:value-of select="normalize-space(title)" /></navtitle>
        </xsl:if>
        <xsl:sequence select="$topicmeta/node()"/>
      </topicmeta>
    </xsl:variable>
    <xsl:variable name="id">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:variable>
    <xsl:variable name="type" select="name(.)"/>
    <xsl:variable name="outdir" select="concat($type, '/')"/>
    <xsl:variable name="href" select="concat($outdir, $id, '.dita')"/>
    <topicref>
      <xsl:if test="$topicref.type">
        <xsl:attribute name="type" select="$type"/>
      </xsl:if>
      <xsl:attribute name="href" select="$href"/>
      <xsl:if test="count($_topicmeta/*) gt 0">
        <xsl:sequence select="$_topicmeta"/>
      </xsl:if>
      <xsl:apply-templates select="." mode="chunk">
        <xsl:with-param name="type" select="$type"/>
        <xsl:with-param name="href" select="$href"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="node()" mode="topicref"/>
      <xsl:sequence select="$topicref"/>
    </topicref>
  </xsl:template>
  
  <!--
    process id attributes
  -->
  <xsl:template match="@id" mode="topicref">
    <xsl:attribute name="id">
      <xsl:apply-templates select="." mode="id"/>
    </xsl:attribute>
  </xsl:template>
  
</xsl:stylesheet>

