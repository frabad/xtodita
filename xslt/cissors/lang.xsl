<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:my="functions:my"
  exclude-result-prefixes="#all">

  <xsl:template match="*" mode="lang" as="xs:string">
    <xsl:value-of select="(ancestor-or-self::*[@xml:lang])[1]/@xml:lang"/>
  </xsl:template>
  
  <xsl:template match="*" mode="attr.lang" as="attribute()">
    <xsl:attribute name="xml:lang">
      <xsl:apply-templates select="." mode="lang" />
    </xsl:attribute>
  </xsl:template>

</xsl:stylesheet>

