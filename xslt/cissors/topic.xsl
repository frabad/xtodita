<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:my="functions:my"
  exclude-result-prefixes="#all">

  <!--
    no output for topics in default mode
  -->
  <xsl:template match="topic | reference | concept | task" />

  <xsl:template match="topic | reference | concept | task" mode="chunk">
    <xsl:param name="type"/>
    <xsl:param name="href"/>
    <xsl:result-document href="{$href}" format="{$type}">
      <xsl:copy>
        <xsl:apply-templates select="." mode="attr.lang" />
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
    </xsl:result-document>
  </xsl:template>

  <xsl:template match="body | refbody | conbody | taskbody">
    <xsl:if
      test="not(preceding-sibling::*[1][self::prolog]) and $prolog.inherit">
      <xsl:sequence select="ancestor::*[child::prolog][1]/prolog"/>
    </xsl:if>
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>

