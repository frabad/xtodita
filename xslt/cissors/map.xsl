<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:my="functions:my"
  exclude-result-prefixes="#all">

  <!--
    apply the root element in map mode
  -->
  <xsl:template match="/">
    <xsl:apply-templates select="*[1]" mode="map"/>
  </xsl:template>

  <xsl:template mode="map" match="map | topic | reference | concept | task">
    <map>
      <xsl:apply-templates select="." mode="attr.lang" />
      <xsl:apply-templates select="@*" />
      <xsl:sequence select="title" />
      <xsl:apply-templates select="node()" mode="topicref"/>
    </map>
  </xsl:template>
  
</xsl:stylesheet>

