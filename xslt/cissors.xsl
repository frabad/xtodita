<?xml version="1.0" ?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:my="functions:my"
  exclude-result-prefixes="#all">

  <rdf:RDF>
    <rdf:Description about="#">
      <dc:title>DITA cissors</dc:title>
      <dc:subject>DITA XML</dc:subject>
      <dc:creator>frabad</dc:creator>
      <dc:created>2015-06-29</dc:created>
      <dc:language>EN</dc:language>
      <dc:description
        >DITA topic chunker and ID/filename normalizer</dc:description>
    </rdf:Description>
  </rdf:RDF>

  <xsl:output
    method="xml"
    encoding="utf-8"
    omit-xml-declaration="no"
    doctype-public="-//OASIS//DTD DITA Map//EN"
    indent="yes"
    doctype-system="map.dtd"
  />
  <xsl:output name="topic"
    method="xml"
    encoding="utf-8"
    indent="no"
    omit-xml-declaration="no"
    doctype-public="-//OASIS//DTD DITA Topic//EN"
    doctype-system="topic.dtd"
  />
  <xsl:output name="task"
    method="xml"
    encoding="utf-8"
    indent="no"
    omit-xml-declaration="no"
    doctype-public="-//OASIS//DTD DITA Task//EN"
    doctype-system="task.dtd"
  />
  <xsl:output
    name="reference"
    method="xml"
    encoding="utf-8"
    indent="no"
    omit-xml-declaration="no"
    doctype-public="-//OASIS//DTD DITA Reference//EN"
    doctype-system="reference.dtd"
  />
  <xsl:output name="concept"
    method="xml"
    encoding="utf-8"
    indent="no"
    omit-xml-declaration="no"
    doctype-public="-//OASIS//DTD DITA Concept//EN"
    doctype-system="concept.dtd"
  />

  <xsl:strip-space elements="*"/>
  
  <xsl:param name="id.rewrite" select="true()"/>
  <xsl:param name="prolog.inherit" select="false()"/>
  <xsl:param name="topicref.type" select="false()"/>
  <xsl:param name="topicref.navtitle" select="true()"/>
  <xsl:param name="unwrap.outputclass"/>
  
  <xsl:include href="copy.xsl" />
  <xsl:include href="cissors/lang.xsl" />
  <xsl:include href="cissors/id.xsl" />
  <xsl:include href="cissors/href.xsl" />
  <xsl:include href="cissors/topic.xsl" />
  <xsl:include href="cissors/topicref.xsl" />
  <xsl:include href="cissors/map.xsl" />
  
</xsl:stylesheet>
