<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">

  <rdf:RDF>
    <rdf:Description rdf:about="#">
      <dc:title
        >XML document to DITA translator</dc:title>
      <dc:description
        >map document content types to their DITA counterparts</dc:description>
      <dc:creator
        >FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>
  
  <xsl:strip-space elements="*" />
  
  <!-- inclusions -->
  
  <xsl:include href="fearless/functions.xsl" />
  <xsl:include href="fearless/defaults.xsl" />
  <xsl:include href="fearless/custom.xsl" />
  <xsl:include href="fearless/topics.xsl" />
  <xsl:include href="fearless/graphics.xsl" />
  <xsl:include href="fearless/tables.xsl" />
  <xsl:include href="fearless/blocks.xsl" />
  <xsl:include href="fearless/lists.xsl" />
  <xsl:include href="fearless/links.xsl" />
  <xsl:include href="fearless/inlines.xsl" />
  
</xsl:stylesheet>

