<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>copy</dc:title>
      <dc:description>
        copy an attribute or a node
      </dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>

  <xsl:template match="attribute() | node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="attribute() | node()"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>


