<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">

  <!-- blocks -->
  
  <xsl:template match="warning|note">
    <note>
      <xsl:if test="self::warning">
        <xsl:attribute name="type" select="local-name()"/>
      </xsl:if>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </note>
  </xsl:template>
  
  <xsl:template match="para
  [
    parent::cell[count(*) = 1]
    or (count(*) = 0 and normalize-space(.) = '')
  ]">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="para">
    <p>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </p>
  </xsl:template>
  
  <xsl:template match="desc">
    <desc>
      <xsl:apply-templates/>
    </desc>
  </xsl:template>
  
  <xsl:template match="example | label | descitem/label/para">
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>

