<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">
  
  <!-- topics -->
  
  <xsl:template match="*[is:topic(.)]" name="topic">
    <xsl:variable name="title">
      <xsl:apply-templates select="*[is:title(.)]"/>
    </xsl:variable>
    <xsl:variable name="topics">
      <xsl:apply-templates select="*[is:topic(.)]"/>
    </xsl:variable>
    <xsl:variable name="body">
      <xsl:apply-templates select="*[not(is:topic(.) or is:title(.))]"/>
    </xsl:variable>
    <topic>
      <xsl:apply-templates select="." mode="topic.attrs"/>
      <xsl:sequence select="$title"/>
      <xsl:if test="not($title/*)">
        <title>
          <xsl:value-of select="concat('[', local-name(.), ']')"/>
        </title>
      </xsl:if>
      <xsl:sequence select="processing-instruction()|comment()" />
      <xsl:if test="$body/*">
        <body>
          <xsl:sequence select="$body"/>
        </body>
      </xsl:if>
      <xsl:sequence select="$topics"/>
    </topic>
  </xsl:template>

  <xsl:template mode="topic.id" match="*">
    <xsl:choose>
      <xsl:when test="@id">
        <xsl:sequence select="@id"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="id">
          <xsl:value-of select="local-name(.),generate-id(.)" separator="-" />
        </xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template mode="topic.attrs" match="*">
    <xsl:apply-templates select="." mode="topic.id"/>
    <xsl:apply-templates select="." mode="generic.attrs"/>
  </xsl:template>
  
  <!-- topic titles -->

  <xsl:template match="*[is:title(.)]">
    <xsl:variable name="label.preceding">
      <xsl:apply-templates
      select="preceding-sibling::*[1][is:label(.)]" mode="label"/>
      <xsl:text> </xsl:text>
    </xsl:variable>
    <xsl:variable name="label.following">
      <xsl:text> </xsl:text>
      <xsl:apply-templates mode="label"
        select="following-sibling::*[1][is:label(.)]"/>
    </xsl:variable>
    <title>
      <xsl:if test="normalize-space($label.preceding)">
        <xsl:sequence select="$label.preceding"/>
      </xsl:if>
      <xsl:apply-templates/>
      <xsl:if test="normalize-space($label.following)">
        <xsl:sequence select="$label.following"/>
      </xsl:if>
    </title>
  </xsl:template>

  <!-- topic title labels -->

  <xsl:template match="*[is:label(.)]" priority="-1"/>

  <xsl:template match="*[is:label(.)]" mode="label">
    <ph>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </ph>
  </xsl:template>

</xsl:stylesheet>

