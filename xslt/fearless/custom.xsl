<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <!-- option parameters -->
  
  <xsl:param name="move.name.to.outputclass" select="false()" />
  <xsl:param name="move.attr.to.otherprops" select="false()" />
  
  <!-- custom variable names -->
  
  <xsl:variable name="topic.names"
  >sect1 sect2 sect3 sect4 preface section chapter daletbook </xsl:variable>
  <xsl:variable name="title.names"> title </xsl:variable>
  <xsl:variable name="label.names"> wtf? </xsl:variable>
  
</xsl:stylesheet>

