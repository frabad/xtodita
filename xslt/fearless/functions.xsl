<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">

  <!-- custom boolean functions -->
  
  <xsl:function name="is:in" as="xs:boolean">
    <xsl:param name="i" as="item()"/>
    <xsl:param name="s" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="local-name($i) = tokenize(normalize-space($s), ' ')">
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="is:topic" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="$e[is:in(., $topic.names)]">
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="is:title" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="$e[is:in(., $title.names)]">
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="is:label" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="is:in($e, $label.names)">
        <xsl:value-of select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="dita:otherprops" as="attribute()">
    <xsl:param name="attrs" as="attribute()*"/>
    <xsl:attribute name="otherprops">
      <xsl:for-each select="$attrs">
        <xsl:value-of select="concat((local-name(.)), '(', ., ')')"/>
        <xsl:if test="position() != last()">
          <xsl:text> </xsl:text>
        </xsl:if>
      </xsl:for-each>
    </xsl:attribute>
  </xsl:function>
  
</xsl:stylesheet>

