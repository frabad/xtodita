<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">

  <!-- graphics -->
  
  <xsl:template match="*" mode="image">
    <xsl:variable name="href" as="xs:string">
      <xsl:variable name="filename" as="xs:string" select="
        if (@color)
        then concat('area-',@color)
        else (@figid,@filename)[1]"
      />
      <xsl:variable name="filetype">
        <xsl:choose>
          <xsl:when test="contains(lower-case(@filetype),'eps')">svg</xsl:when>
          <xsl:when test="contains(lower-case(@filetype),'png')">png</xsl:when>
          <xsl:when test="contains(lower-case(@filetype),'jpg')">jpg</xsl:when>
          <xsl:otherwise>svg</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:value-of select="concat('../img/',$filename,'.',$filetype)" />
    </xsl:variable>
    <image>
      <xsl:attribute name="href" select="$href" />
      <xsl:if test="
        not(self::area) and
        parent::*[count(*) = 1
        and normalize-space(.) = '']">
        <xsl:attribute name="placement">break</xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <alt><xsl:value-of select="$href"/></alt>
    </image>
  </xsl:template>
  
  <xsl:template match="figure">
    <xsl:param name="desc" as="item()*" />
    <fig id="{@figid}">
      <xsl:apply-templates select="title" />
      <xsl:if test="$desc">
        <desc>
          <xsl:sequence select="$desc"/>
        </desc>
      </xsl:if>
      <xsl:apply-templates select="." mode="image" />
    </fig>
  </xsl:template>

  <xsl:template match="description[figure][not(table)]">
    <xsl:apply-templates select="figure">
      <xsl:with-param name="desc">
        <xsl:apply-templates select="*[not(self::figure)]" />
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>
  
  <xsl:template match="description[figure][table]">
    <xsl:apply-templates select="figure" />
    <xsl:apply-templates select="*[not(self::figure)]" />
  </xsl:template>
  
  <xsl:template match="catalog|description">
    <xsl:apply-templates />
  </xsl:template>
  
  <xsl:template match="picture">
    <xsl:apply-templates select="." mode="image" />
  </xsl:template>

  <xsl:template match="area">
    <xsl:apply-templates select="." mode="image" />
  </xsl:template>

</xsl:stylesheet>

