<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">

  <!-- tables -->

  <xsl:template match="table">
    <xsl:apply-templates select="." mode="simpletable"/>
  </xsl:template>

  <xsl:template match="table" mode="cals">
    <table>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </table>
  </xsl:template>
  
  <!-- simple table -->

  <xsl:template match="table" mode="simpletable">
    <simpletable>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates mode="simpletable"/>
    </simpletable>
  </xsl:template>

  <xsl:template match="thead" mode="simpletable">
    <sthead>
      <xsl:apply-templates select="row/cell" mode="simpletable"/>
    </sthead>
  </xsl:template>

  <xsl:template match="tbody" mode="simpletable">
    <xsl:apply-templates mode="simpletable"/>
  </xsl:template>

  <xsl:template match="row" mode="simpletable">
    <strow>
      <xsl:apply-templates mode="simpletable"/>
    </strow>
  </xsl:template>

  <xsl:template match="cell" mode="simpletable">
    <stentry>
      <xsl:apply-templates/>
    </stentry>
  </xsl:template>

</xsl:stylesheet>

