<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">

  <!-- lists -->

  <xsl:template match="procedure | sublist[ancestor::procedure]">
    <ol>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </ol>
  </xsl:template>

  <xsl:template match="list | sublist[ancestor::list]">
    <ul>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </ul>
  </xsl:template>
  
  <xsl:template match="item">
    <li>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </li>
  </xsl:template>

  <xsl:template match="item/*[is:label(.)]">
    <xsl:apply-templates select="." mode="label"/>
  </xsl:template>

  <xsl:template match="figdesc">
    <dl>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates select="descitem" />
    </dl>
  </xsl:template>
  
  <xsl:template match="descitem">
    <dlentry>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <dt><xsl:apply-templates select="label" /></dt>
      <dd><xsl:apply-templates select="desc" /></dd>
    </dlentry>
  </xsl:template>
  
  <xsl:template match="descitem/label | descitem/desc">
    <xsl:apply-templates />
  </xsl:template>
  
</xsl:stylesheet>

