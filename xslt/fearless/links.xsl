<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">
  
  <xsl:key name="id" match="*[@id]" use="@id"/>
  <xsl:key name="title" match="*[@id]" use="*[is:title(.)]"/>
  
  <xsl:template match="url" mode="__draft__">
    <xsl:variable name="url" select="(@url, .)[1]"/>
    <xref href="{$url}">
      <xsl:attribute name="scope">external</xsl:attribute>
      <xsl:attribute name="format">
        <xsl:choose>
          <xsl:when test="ends-with(lower-case($url), '.pdf')">pdf</xsl:when>
          <xsl:otherwise>html</xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates select="." mode="generic.attrs"/>
      <xsl:apply-templates/>
    </xref>
  </xsl:template>

  <xsl:template match="ref" mode="__draft__">
    <xsl:variable name="refid" select="key('title', @no)/@id"/>
    <xsl:choose>
      <xsl:when test="$refid">
        <xref href="#{$refid}">
          <xsl:apply-templates select="." mode="generic.attrs"/>
          <xsl:apply-templates/>
        </xref>
      </xsl:when>
      <xsl:otherwise>
        <ph>
          <xsl:apply-templates select="." mode="generic.attrs"/>
          <xsl:apply-templates/>
        </ph>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="footnote" mode="__draft__">
    <xsl:variable name="reftopic"
        select="key('id', @refid)/ancestor-or-self::*[is:topic(.)][1]/@id"/>
    <xsl:choose>
      <xsl:when test="$reftopic != ''">
        <xref href="{concat('#',$reftopic,'/',@refid)}" type="fn">
          <xsl:apply-templates select="." mode="generic.attrs"/>
        </xref>
      </xsl:when>
      <xsl:otherwise>
        <ph>
          <xsl:apply-templates select="." mode="generic.attrs"/>
          <xsl:apply-templates/>
        </ph>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

