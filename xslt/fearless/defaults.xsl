<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:dita="http://dita.oasis-open.org/architecture/2005/"
  xmlns:is="namespace:is"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf dita xs is">
  
  <!-- normalize attributes -->
  
  <xsl:template mode="generic.attrs" match="*">
    <xsl:variable name="attrs" select="@*[not(local-name(.) = ('id','lang'))]"/>
    <xsl:if test="$move.name.to.outputclass">
      <xsl:attribute name="outputclass" select="local-name(.)"/>
    </xsl:if>
    <xsl:if test="$move.attr.to.otherprops and not(empty($attrs))">
      <xsl:sequence select="dita:otherprops($attrs)"/>
    </xsl:if>
    <xsl:sequence select="@id"/>
    <xsl:if test="@lang">
      <xsl:attribute name="xml:lang" select="@lang" />
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="processing-instruction()|comment()">
    <xsl:sequence select="."/>
  </xsl:template>

  <!-- unmatched elements output a message -->
  
  <xsl:template match="element()">
    <xsl:variable name="msg" as="xs:string"
        select="concat('ignored: ',local-name(.))" />
    <xsl:message select="$msg" />
    <xsl:comment><xsl:value-of select="$msg" /></xsl:comment>
  </xsl:template>

</xsl:stylesheet>

