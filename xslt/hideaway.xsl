<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  exclude-result-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>fix</dc:title>
      <dc:description
        >fix a node when needed</dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>

  <xsl:include href="copy.xsl" />
  
  <!-- unwrap fake table to admonition -->
  <xsl:template match="simpletable[
    count(strow[1]/stentry)=1 and strow[1]/stentry/image]">
    <xsl:variable name="utype" select="lower-case(normalize-space(
      strow[1]/stentry/image/following-sibling::element()[1]
    ))"/>
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="$utype=
          ('precauzione','cauzione')">caution</xsl:when>
        <xsl:when test="$utype=
          ('pericolo','danger')">danger</xsl:when>
        <xsl:when test="$utype=
          ('avvertenza','avertissement','warning')">warning</xsl:when>
        <xsl:when test="$utype=
          ('importante','important')">important</xsl:when>
        <xsl:when test="$utype=
          ('attenzione','attention')">attention</xsl:when>
        <xsl:when test="$utype=
          ('nota','note')">note</xsl:when>
        <xsl:otherwise
          >othertype</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="othertype"
      select="if ($type='othertype') then $utype else ()"
    />
    <note>
      <xsl:if test="$type != 'note'">
        <xsl:attribute name="type" select="normalize-space($type)" />
      </xsl:if>
      <xsl:if test="$othertype">
        <xsl:attribute name="othertype" select="normalize-space($othertype)" />
      </xsl:if>
      <xsl:apply-templates select="
        for $i in strow[position() gt 1]
        return $i/stentry/node()"
      />
    </note>
  </xsl:template>

  <xsl:template match="taskbody" >
    <xsl:variable as="element()+" name="contents">
      <context>
        <xsl:apply-templates mode="#current"
          select="node()[following-sibling::steps]" />
      </context>
      <xsl:apply-templates select="steps" />
      <example>
        <xsl:apply-templates select="fig[preceding-sibling::steps]" />
      </example>
      <postreq>
        <xsl:apply-templates select="node()[preceding-sibling::steps][not(self::fig)]" />
      </postreq>
    </xsl:variable>
    <xsl:copy>
      <xsl:sequence select="@*"/>
      <xsl:sequence select="for $i in $contents return $i[*]" />
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>


