<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  extension-element-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>all names to lower-case</dc:title>
      <dc:description
        >convert all input nodes/attr-nodes names to lower case</dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>
  
  <xsl:include href="copy.xsl" />
  
  <xsl:template match="element()">
    <xsl:element name="{lower-case(local-name(.))}">
      <xsl:apply-templates select="attribute() | node()"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="attribute()">
    <xsl:attribute name="{lower-case(local-name(.))}">
      <xsl:value-of select="."/>
    </xsl:attribute>
  </xsl:template>

</xsl:stylesheet>

