<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  extension-element-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>processing-instructions utilities</dc:title>
      <dc:description
        >utility functions to parse, query and consume processing-instructions
      </dc:description>
    </rdf:Description>
  </rdf:RDF>

  <!--docbook-xsl function for retrieving PI pseudo-attrs-->
  
  <xsl:template name="pi-attribute">
    <xsl:param name="pis" />
    <xsl:param name="attribute">attr</xsl:param>
    <xsl:param name="count">1</xsl:param>
    <xsl:choose>
      <xsl:when test="$count&gt;count($pis)">
        <!-- not found -->
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="pi" select="$pis[$count]"/>
        <xsl:variable name="pivalue"
          select="concat(' ', normalize-space($pi))"/>
        <xsl:choose>
          <xsl:when test="contains($pivalue,concat(' ', $attribute, '='))">
            <xsl:variable name="rest"
              select="substring-after($pivalue,concat(' ', $attribute,'='))"/>
            <xsl:variable name="quote"
              select="substring($rest,1,1)"/>
            <xsl:value-of select="substring-before(substring($rest,2),$quote)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="pi-attribute">
              <xsl:with-param name="pis" select="$pis"/>
              <xsl:with-param name="attribute" select="$attribute"/>
              <xsl:with-param name="count" select="$count + 1"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--parse a dita PI-->
  <xsl:template name="topic.pi" as="element(toPIc)?">
    <xsl:variable name="target"
      select="self::*/processing-instruction('dita')" />
    <xsl:variable name="type">
      <xsl:call-template name="pi-attribute">
        <xsl:with-param name="attribute" select="'type'"/>
        <xsl:with-param name="pis" select="$target"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="mode">
      <xsl:call-template name="pi-attribute">
        <xsl:with-param name="attribute" select="'mode'"/>
        <xsl:with-param name="pis" select="$target"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="normalize-space(concat($type,$mode))">
      <toPIc>
        <xsl:if test="$type">
          <xsl:attribute name="type" select="normalize-space($type)"/>
        </xsl:if>
        <xsl:if test="$mode">
          <xsl:attribute name="mode" select="normalize-space($mode)"/>
        </xsl:if>
      </toPIc>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>

