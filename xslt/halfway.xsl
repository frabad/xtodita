<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  extension-element-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>apply fixes to the input tree</dc:title>
      <dc:description
        >reorganize/improve the input tree</dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>
  
  <xsl:include href="copy.xsl" />
  <xsl:include href="pi.xsl" />
  
  <xsl:template match="node()" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="attribute()|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="*[title]">
    <xsl:variable name="toPIc" as="element()?">
      <xsl:call-template name="topic.pi" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$toPIc/@type='task' and $toPIc/@mode='1'">
        <xsl:apply-templates select="." mode="task-alt-1" />
      </xsl:when>
      <xsl:when test="$toPIc/@type='task' and $toPIc/@mode='2'">
        <xsl:apply-templates select="." mode="task-alt-2" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="attribute()|node()" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template mode="task-alt-1" match="*[title]">
    <xsl:copy>
      <xsl:sequence select="attribute()|title|processing-instruction()"/>
      <procedure>
        <xsl:apply-templates mode="#current"
          select="element()[not(self::title)]" />
      </procedure>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template mode="task-alt-1" match="description">
    <item>
      <xsl:apply-templates mode="#current" select="node()" />
    </item>
  </xsl:template>
  
  <xsl:template mode="task-alt-1" match="description/list[count(item)=1]">
    <xsl:apply-templates mode="#current" select="item/node()" />
  </xsl:template>
  
  <xsl:template mode="task-alt-1" match="description/list[count(item) gt 1]">
    <procedure>
      <xsl:apply-templates mode="#current" select="item" />
    </procedure>
  </xsl:template>
  
  <xsl:template mode="task-alt-2" match="*[title]">
    <xsl:copy>
      <xsl:sequence select="attribute()|title|processing-instruction()"/>
      <xsl:apply-templates mode="#current"
        select="element()[not(self::title)]"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template mode="task-alt-2" match="description">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template mode="task-alt-2" match="list|procedure">
    <procedure>
      <xsl:apply-templates mode="#current"/>
    </procedure>
  </xsl:template>
  
</xsl:stylesheet>

