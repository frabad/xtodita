<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  extension-element-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>convert topics to concepts</dc:title>
      <dc:description
        >convert marked DITA topics to concepts</dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>
  
  <xsl:template match="topic" mode="concept">
    <concept>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()[not(self::topic)]" mode="#current" />
      <xsl:apply-templates select="topic" mode="#default" />
    </concept>
  </xsl:template>
  
  <xsl:template match="body" mode="concept">
    <conbody>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()" mode="#current" />
    </conbody>
  </xsl:template>
    
</xsl:stylesheet>

