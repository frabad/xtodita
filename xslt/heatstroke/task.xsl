<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  extension-element-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>convert topics to tasks</dc:title>
      <dc:description>
        convert marked DITA topics to tasks
      </dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>
  
  <xsl:template match="topic" mode="task">
    <task>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()[not(self::topic)]" mode="#current" />
      <xsl:apply-templates select="topic" mode="#default" />
    </task>
  </xsl:template>
  
  <xsl:template match="body" mode="task">
    <taskbody>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </taskbody>
  </xsl:template>
  
  <xsl:template match="ol" mode="task">
    <steps>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates mode="#current" select="node()" />
    </steps>
  </xsl:template>
  
  <xsl:template match="li/ol" mode="task">
    <substeps>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()" mode="#current" />
    </substeps>
  </xsl:template>
  
  <xsl:template match="li/ul" mode="task">
    <choices>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()" mode="#current" />
    </choices>
  </xsl:template>
  
  <xsl:template match="li/ul/li" mode="task">
    <choice>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()" mode="#current" />
    </choice>
  </xsl:template>
  
  <xsl:template match="ol/li" mode="task">
    <xsl:element name="{if (ancestor::li) then 'substep' else 'step'}">
      <xsl:sequence select="@*"/>
      <cmd>
        <xsl:apply-templates mode="#current" select="p[1]/node() | text()"/>
      </cmd>
      <xsl:if test="count(p) gt 1 or
        element()[not(self::p or self::ul or self::ol)]">
        <info>
          <xsl:apply-templates mode="#current"
            select="node()[not(self::p[1] or self::ul or self::ol)]"
          />
        </info>
      </xsl:if>
      <xsl:apply-templates mode="#current" select="ul | ol" />
    </xsl:element>
  </xsl:template>
  
</xsl:stylesheet>

