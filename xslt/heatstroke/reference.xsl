<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  extension-element-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>convert topics to references</dc:title>
      <dc:description>
        convert marked DITA topics to references
      </dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>

  <!--rewrap references-->
  
  <xsl:template match="topic" mode="reference">
    <reference>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()[not(self::topic)]" mode="#current" />
      <xsl:apply-templates select="topic" mode="#default" />
    </reference>
  </xsl:template>
  
  <xsl:template match="body" mode="reference">
    <refbody>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()" mode="#current" />
    </refbody>
  </xsl:template>
  
  
</xsl:stylesheet>

