<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet  version="2.0"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  extension-element-prefixes="dc rdf"
>
  <rdf:RDF>
    <rdf:Description rdf:about="">
      <dc:title>convert DITA topics</dc:title>
      <dc:description
        >Convert marked DITA topics to other types</dc:description>
      <dc:creator>FBA</dc:creator>
    </rdf:Description>
  </rdf:RDF>

  <xsl:include href="copy.xsl" />
  <xsl:include href="pi.xsl" />
  <xsl:include href="heatstroke/concept.xsl" />
  <xsl:include href="heatstroke/reference.xsl" />
  <xsl:include href="heatstroke/task.xsl" />
  
  <!--consume/cleanup dita PIs-->
  <xsl:template match="processing-instruction('dita')" mode="#all" />
  
  <xsl:template match="node()" mode="topic reference concept task">
    <xsl:copy>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates select="node()" />
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="topic">
    <xsl:variable name="toPIc" as="element()?">
      <xsl:call-template name="topic.pi" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="normalize-space(body) = ''">
        <xsl:apply-templates select="." mode="topic" />
      </xsl:when>
      <xsl:when test="body/procedure or $toPIc/@type='task'">
        <xsl:apply-templates select="." mode="task" />
      </xsl:when>
      <xsl:when test="false()=(for $i in body/element()
        return name($i)!=('table','simpletable','example')) ">
        <xsl:apply-templates select="." mode="reference" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="." mode="concept" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>

